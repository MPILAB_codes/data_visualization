# BVTK (Uisng vtk in Blender)

[PROJECT SOURCE](https://github.com/esowc/sci_vis)
- This project is for directly using vtk in Blender.
- It is only suitable for Blender 2.79 and VTK version 7 above 

##### Installation 
1. Install Blender 2.79 
2. Download the source code zip file 
3. Blender menu > User Preferences > Addons > install from file > choose the zip-archive > activate flag beside BVTK （In the bottom left of the window click on save user settings. By doing this you'll enable the add-on once for all and you won't have to enable it each time you open Blender, which is annoying and confusing if you are new to the software.）

##### Get Started 
1. Open blender from the terminal.
 In the top bar choose file > open and locate the file you want to visulize.
 In the top right of the window click open blender file.
2. Create your data flow using different Nodes.



