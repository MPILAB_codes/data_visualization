#!/usr/bin/env python

# please add the following to your bashrc/bash_profile file (modifying it to your case):
# PATH=${HYBRIDR_DIR}/utilities/:$PATH
# where 'HYBRIDR_DIR' points to your local
# /Applications/VisIt.app/Contents/Resources/bin/visit

import os
import sys

sys.path.append("/Applications/VisIt.app/Contents/Resources/3.1.0/darwin-x86_64/lib/site-packages")
from visit import *
# import pdb
import os.path
# import visit_writer
import time as mytime

inputargs = sys.argv
scriptname = inputargs[0]
file_extension = "tec"

if len(inputargs) < 2:
    syserror = " Syntax should be: visit -cli -nowin -s  <input/output_binary_folder>\n"
    syserror += " where: <input_binary_folder> = location of your .tec files \n"
    sys.exit(syserror)

input_binary_folder = inputargs[1]

dot_file_extension = "." + file_extension
listall = list(set(os.listdir(input_binary_folder)))
filename_list = []

for filename in listall:
    if filename[-(len(dot_file_extension)):] == dot_file_extension:
        if not "isoX" in filename:
            filename_list.append(filename)

# Reading all the variables in .res files and converting .res files to .h5 files
for filename in filename_list:
    print("\n Converting " + filename + " to extract isosurface...")

    tec_filename = input_binary_folder + "/" + filename.replace(dot_file_extension, "tec")
    tick1 = mytime.time()
    OpenDatabase(tec_filename, 0)
    tick2 = mytime.time()
    print("read in file: ", tick2 - tick1)

    AddPlot("Pseudocolor", "w", 1, 1)
    AddOperator("Isosurface", 1)
    SetActivePlots(0)
    IsosurfaceAtts = IsosurfaceAttributes()
    IsosurfaceAtts.contourValue = (0.0025)
    IsosurfaceAtts.contourMethod = IsosurfaceAtts.Value  # Level, Value, Percent
    IsosurfaceAtts.variable = "vort"
    DrawPlots()

    SetOperatorOptions(IsosurfaceAtts, 1)
    SaveWindowAtts = SaveWindowAttributes()
    SaveWindowAtts.outputToCurrentDirectory = 1
    SaveWindowAtts.outputDirectory = "input_binary_folder"
    SaveWindowAtts.fileName = tec_filename[:-4] + "_isoVort.ply"
    SaveWindowAtts.family = 0
    SaveWindowAtts.format = SaveWindowAtts.PLY  # BMP, CURVE, JPEG, OBJ, PNG, POSTSCRIPT, POVRAY, PPM, RGB, STL, TIFF, ULTRA, VTK, PLY
    SaveWindowAtts.width = 1024
    SaveWindowAtts.height = 1024
    SaveWindowAtts.screenCapture = 0
    SaveWindowAtts.saveTiled = 0
    SaveWindowAtts.quality = 80
    SaveWindowAtts.progressive = 0
    SaveWindowAtts.binary = 0
    SaveWindowAtts.stereo = 0
    SaveWindowAtts.compression = SaveWindowAtts.PackBits  # None, PackBits, Jpeg, Deflate
    SaveWindowAtts.forceMerge = 0
    SaveWindowAtts.resConstraint = SaveWindowAtts.ScreenProportions  # NoConstraint, EqualWidthHeight, ScreenProportions
    SaveWindowAtts.advancedMultiWindowSave = 0
    SetSaveWindowAttributes(SaveWindowAtts)
    SaveWindow()
    tick3 = mytime.time()
    print("saved: ", tick3 - tick2)
