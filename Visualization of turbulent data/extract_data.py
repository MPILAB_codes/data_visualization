import ioHybrid as io
import tecplot_WriteRectilinearMesh as tp
import numpy as np


print ('STARING')

muRef=0.0001176471
TRef=1.0
R=17.8571
gamma=1.4
myFile='/Users/j6hickey/Downloads/Re480_n3105966_t8.760e+02.prim'
x,y,z,field,var=io.binary_io(myFile) #Read snapshot variables
Nx=np.size(x)
Ny=np.size(y)
Nz=np.size(z)
Ntot=Nx*Ny*Nz
print Ntot

    #We upload the prim files
rho=np.reshape(field[0:Ntot],[Nx,Ny,Nz])
U=np.reshape(field[Ntot:Ntot*2],[Nx,Ny,Nz])
V=np.reshape(field[Ntot*2:Ntot*3],[Nx,Ny,Nz])
W=np.reshape(field[Ntot*3:Ntot*4],[Nx,Ny,Nz])
P=np.reshape(field[Ntot*4:Ntot*5],[Nx,Ny,Nz])
T=P/(rho*R)


print ('MODIFIED SIZE OF FILES FOR TESTING!!!!!!')
x=x[:10]
y=y[:12]
z=z[:14]
print ('MODIFIED SIZE OF FILES FOR TESTING!!!!!!')


# Convert Coordinate to list
X = []
for i in xrange(len(x)):
    X=X+[x[I]]
Y = []
for j in xrange(len(y)):
    Y=Y+[y[j]]
Z = []
for k in xrange(len(z)):
    Z=Z+[z[k]]

# Data
Ulist = []
Vlist = []
nodeid =[]
id = 0
for k in xrange(len(z)):
    for j in xrange(len(y)):
        for i in xrange(len(x)):
           Ulist = Ulist + [U[i,j,k]]
           Vlist = Vlist + [V[i,j,k]]
           nodeid = nodeid + [id]
           id = id + 1


# Write the data to Tecplot format
print('CONVERT TO TECPLOT')

vars = (("U", Ulist), ("V", Vlist),("nodeid", nodeid))
tp.tecplot_WriteRectilinearMesh("3drect.tec", X, Y, Z, vars) # 3D
