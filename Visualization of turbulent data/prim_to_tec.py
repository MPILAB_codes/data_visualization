import numpy as np
import struct

prim_file = 'Re480_n4178858_t1.174e+03.prim'
tec_file = 'output.tec'

def tecplot_WriteRectilinearMesh(filename, X, Y, Z, vars):
    def pad(s, width):
        s2 = s
        while len(s2) < width:
            s2 = ' ' + s2
        if s2[0] != ' ':
            s2 = ' ' + s2
        if len(s2) > width:
            s2 = s2[:width]
        return s2

    def varline(vars, id, fw):
        s = ""
        print ("in progress")
        s = s + pad(str(vars[1][id]), fw)
        s = s + '\n'
        print(s)
        return s

    fw = 20  # field width

    f = open(filename, "wt")

    f.write('Variables="X","Y","Z"')
    f.write(',"%s"' % vars[0])
    f.write('\n\n')

    f.write('Zone I=' + pad(str(len(X)), 6) + ',J=' + pad(str(len(Y)), 6) + ',K=' + pad(str(len(Z)), 6))
    f.write(', F=POINT\n')

    id = 0
    for k in range(len(Z)):
        for j in range(len(Y)):
            for i in range(len(X)):
                f.write(pad(str(X[i]), fw) + pad(str(Y[j]), fw) + pad(str(Z[k]), fw))
                f.write(varline(vars, id, fw))
                id = id + 1

    f.close()



def binary_io(filename):
    f = open(filename, 'rb')
    double = 8
    integer = 4
    endian = '<'

    # Read integer header
    NIntegerHeader = 7
    tmp = f.read(NIntegerHeader * integer)
    Precision, version, step, Nx, Ny, Nz, Nvars = np.array(
        struct.unpack(endian + str(NIntegerHeader) + 'i', tmp))

    # Variable identifiers
    tmp = f.read(integer * Nvars)
    Var = np.array(struct.unpack(endian + str(Nvars) + 'i', tmp))

    # Read Real Header
    if Precision == double:
        real = 'd'
    else:
        real = 'f'

    time = np.array(struct.unpack('<d', f.read(double)))
    # Coordinate
    x = np.array(struct.unpack(endian + str(Nx) + 'd', f.read(double * Nx)))
    y = np.array(struct.unpack(endian + str(Ny) + 'd', f.read(double * Ny)))
    z = np.array(struct.unpack(endian + str(Nz) + 'd', f.read(double * Nz)))

    # Read field
    TotalP = Nx * Ny * Nz
    field = np.array(struct.unpack(endian + str(TotalP * Nvars) + real, f.read(Precision * TotalP * Nvars)))

    # Close file
    f.close()

    return x, y, z, field, Var


X = binary_io(prim_file)[0]
Y = binary_io(prim_file)[1]
Z = binary_io(prim_file)[2]
vars = ("field", binary_io(prim_file)[3])
#
tecplot_WriteRectilinearMesh(tec_file, X, Y, Z, vars)
