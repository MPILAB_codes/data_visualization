from ioHybrid import binary_io
import numpy as np
from generate_isosurface import *

prim_file = 'Re480_n4178858_t1.174e+03.prim'
muRef = 0.0001176471
TRef = 1.0
R = 17.8571
gamma = 1.4

x, y, z, field, var = binary_io(prim_file)
Nx = np.size(x)
Ny = np.size(y)
Nz = np.size(z)
Ntot = Nx*Ny*Nz

rho = np.reshape(field[0:Ntot], [Nx, Ny, Nz])
U = np.reshape(field[Ntot:Ntot*2], [Nx, Ny, Nz])
V = np.reshape(field[Ntot*2:Ntot*3], [Nx, Ny, Nz])
W = np.reshape(field[Ntot*3:Ntot*4], [Nx, Ny, Nz])
P = np.reshape(field[Ntot*4:Ntot*5], [Nx, Ny, Nz])
T = P/(rho*R)

iso_x = []
iso_y = []
iso_z = []
#
# for i in range(len(verts)):
#     iso_x.append(verts[i][0])
#     iso_y.append(verts[i][1])
#     iso_z.append(verts[i][2])
#
#
# gridToVTK("./iso", iso_x, iso_y, iso_z)

print(x)