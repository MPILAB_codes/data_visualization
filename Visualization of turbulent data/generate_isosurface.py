import numpy as np
from skimage import measure

import struct

prim_file = 'Re480_n4178858_t1.174e+03.prim'
muRef = 0.0001176471
TRef = 1.0
R = 17.8571
gamma = 1.4

def binary_io(filename):
    f = open(filename, 'rb')
    double = 8
    integer = 4
    endian = '<'

    # Read integer header
    NIntegerHeader = 7
    tmp = f.read(NIntegerHeader * integer)
    Precision, version, step, Nx, Ny, Nz, Nvars = np.array(
        struct.unpack(endian + str(NIntegerHeader) + 'i', tmp))

    # Variable identifiers
    tmp = f.read(integer * Nvars)
    Var = np.array(struct.unpack(endian + str(Nvars) + 'i', tmp))

    # Read Real Header
    if Precision == double:
        real = 'd'
    else:
        real = 'f'

    time = np.array(struct.unpack('<d', f.read(double)))

    # Coordinate
    x = np.array(struct.unpack(endian + str(Nx) + 'd', f.read(double * Nx)))
    y = np.array(struct.unpack(endian + str(Ny) + 'd', f.read(double * Ny)))
    z = np.array(struct.unpack(endian + str(Nz) + 'd', f.read(double * Nz)))

    #x_axis, y_axis, z_axis = np.vstack(np.meshgrid(x, y, z))

    # Read field
    TotalP = Nx * Ny * Nz
    field = np.array(struct.unpack(endian + str(TotalP * Nvars) + real, f.read(Precision * TotalP * Nvars)))

    f.close()

    return x, y, z, field, Var


x, y, z, field, var = binary_io(prim_file)
Nx = np.size(x)
Ny = np.size(y)
Nz = np.size(z)
Ntot = Nx*Ny*Nz

rho = np.reshape(field[0:Ntot], [Nx, Ny, Nz])
U = np.reshape(field[Ntot:Ntot*2], [Nx, Ny, Nz])
V = np.reshape(field[Ntot*2:Ntot*3], [Nx, Ny, Nz])
W = np.reshape(field[Ntot*3:Ntot*4], [Nx, Ny, Nz])
P = np.reshape(field[Ntot*4:Ntot*5], [Nx, Ny, Nz])
T = P/(rho*R)

#x_axis, y_axis, z_axis = np.vstack(np.meshgrid(Nx, Ny, Nz))
#
verts, faces, normals, values = measure.marching_cubes_lewiner(
    P,
    level=None,
    spacing=(0.1, 0.1, 0.1),
    gradient_direction='descent',
    step_size=1,
    allow_degenerate=True,
    use_classic=False)

verts2, faces2, normals2, values2 = measure.marching_cubes_lewiner(
    T,
    level=None,
    spacing=(0.1, 0.1, 0.1),
    gradient_direction='descent',
    step_size=1,
    allow_degenerate=True,
    use_classic=False)

print(verts)
print(verts[0][1])
print(len(verts))

# fig = plt.figure()
#
# ax = fig.add_subplot(111, projection='3d')
#
# ax.plot_trisurf(verts[:, 0], verts[:, 1], faces, verts[:, 2],
#                 cmap='Spectral', lw=1)
# ax.plot_trisurf(verts2[:, 0], verts2[:, 1], faces2, verts2[:, 2],
#                  cmap='Spectral', lw=1)
#
# plt.show()

