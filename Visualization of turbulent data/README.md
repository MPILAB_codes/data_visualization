## Visualization of turbulent data 
Scripts in this folder are methods to extract isosurface data using mayavi and scikit. Also, methods used to write prim file to tec file.


#### File Structure
| -- -- tec2vtk.dSYM

| -- -- VTK   
        
 
        | -- -- extract_data.py
        | -- -- extractIsosurfaceVisit.py 
        | -- -- generate_isosurface.py *
        | -- -- ioHybrid.py 
        | -- -- isosurface_mayavi.py 
        | -- -- mayavi_vtk.py 
        | -- -- prim_to_tec.py 
        | -- -- tecplot_WriteRectilinearMesh.py 
        | -- -- unstructured_vtk_read.py *
        | -- -- write_vtk.py 

### Requirements and Environment
- macOS Catalina 10.15.4
- Python > 3.0 (3.7)
- Anaconda 3
- conda install -c anaconda mayavi
- conda install -c intel scikit-learn
- conda install -c anaconda pyqt

(more details on installing mayavi:
 1. conda create --name myenv python=3 # build virtual env named myenv
 2. conda update -n base conda # active the env
 3. brew install vtk --with-python3 --without-python
 4. conda install -c anaconda mayavi )

### Usage
1. Tecplot write into an array then use mayavi get isosurface
2. Use mayavi read vtk file and get isosurface

main features:
generate_isosurface.py: input an prim -> get figure of isosurface 
```sh 
python3 generate_isosurface.py
```
unstructured_vtk_read.py
```sh
python3 unstructured_vtk_read.py --pylab qt 
```



