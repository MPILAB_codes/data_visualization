import os
from os.path import join, abspath, dirname
import mayavi
from mayavi.scripts import mayavi2
from mayavi.sources.vtk_file_reader import VTKXMLFileReader
from mayavi.modules.outline import Outline
from mayavi.modules.iso_surface import IsoSurface


filename = '.bottomWall.vtp'
base = os.path.basename('bottomWall.vtp')


def setup_data(filename):
    src = VTKXMLFileReader()
    src.initialize(filename)
    mayavi2.add_source(src)

    return src


def isosurface():
    """Sets up the mayavi pipeline for the visualization.
    """
    # Create an outline for the data.
    o = Outline()
    mayavi2.add_module(o)

    i = IsoSurface()
    mayavi2.add_module(i)
    i.contour.contours[0] = 550
    i.actor.property.opacity = 0.5


@mayavi2.standalone
def main():
    mayavi.new_scene()

    data_dir = mayavi2.get_data_dir(dirname(abspath(__file__)))
    fname = join(data_dir, 'bottomWall.vtu')
    r = setup_data(fname)


if __name__ == '__main__':
    main()

# mlab.test_contour3d()
# mlab.show()
