from ioHybrid import binary_io
import numpy as np
import re
from mayavi import mlab

prim_file = 'Re480_n4178858_t1.174e+03.prim'
muRef = 0.0001176471
TRef = 1.0
R = 17.8571
gamma = 1.4

x, y, z, field, var = binary_io(prim_file)
Nx = np.size(x)
Ny = np.size(y)
Nz = np.size(z)
Ntot = Nx*Ny*Nz

rho = np.reshape(field[0:Ntot], [Nx, Ny, Nz])
U = np.reshape(field[Ntot:Ntot*2], [Nx, Ny, Nz])
V = np.reshape(field[Ntot*2:Ntot*3], [Nx, Ny, Nz])
W = np.reshape(field[Ntot*3:Ntot*4], [Nx, Ny, Nz])
P = np.reshape(field[Ntot*4:Ntot*5], [Nx, Ny, Nz])
T = P/(rho*R)

src = mlab.pipeline.scalar_field(T)
mlab.pipeline.iso_surface(src)

mlab.show()

