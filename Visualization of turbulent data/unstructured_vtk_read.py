from mayavi import mlab
from mayavi.modules.iso_surface import IsoSurface

file_name = './VTK/flow_VTK.vtk'

engine = mlab.get_engine()
vtk_file_data = engine.open(file_name)
iso = IsoSurface()
engine.add_filter(iso, vtk_file_data)
extracted_source = engine.scenes[0].children[0]
print(extracted_source)
mlab.pipeline.get_vtk_src(extracted_source)
mlab.show()

