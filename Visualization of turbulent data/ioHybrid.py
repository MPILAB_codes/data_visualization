# The postprocessing of the turbulent channel flow simulated by HybridX
import numpy as np
import struct

file_name = 'Re480_n4178858_t1.174e+03.prim'

def binary_io(filename):

    f = open(filename, 'rb')
    single = 4
    double = 8
    integer = 4
    endian = '<'

    # Read integer header
    NIntegerHeader = 7
    tmp = f.read(NIntegerHeader * integer)
    Precision, version, step, Nx, Ny, Nz, Nvars = np.array(
        struct.unpack(endian + str(NIntegerHeader) + 'i', tmp))
    # Variable identifiers
    tmp = f.read(integer * Nvars)
    Var = np.array(struct.unpack(endian + str(Nvars) + 'i', tmp))

    # Read Real Header
    if Precision == double:
        real = 'd'
    else:
        real = 'f'

    time = np.array(struct.unpack('<d', f.read(double)))
    # Coordinate
    x = np.array(struct.unpack(endian + str(Nx) + 'd', f.read(double * Nx)))
    y = np.array(struct.unpack(endian + str(Ny) + 'd', f.read(double * Ny)))
    z = np.array(struct.unpack(endian + str(Nz) + 'd', f.read(double * Nz)))

    # Read field
    TotalP = Nx * Ny * Nz
    field = np.array(struct.unpack(endian + str(TotalP * Nvars) + real, f.read(Precision * TotalP * Nvars)))

    f.close()

    return x, y, z, field, Var

