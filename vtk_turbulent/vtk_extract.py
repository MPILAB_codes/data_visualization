import vtk

# generate iso-surface data
def main():

    file_name = get_program_parameters()

    # Read the source file.
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(file_name)
    reader.Update()
    output = reader.GetOutput()

    # generate iso-surface
    iso = vtk.vtkContourFilter()
    iso.SetInputConnection(reader.GetOutputPort())
    # iso.SetValue(0, 0.241629)
    # iso.SetValue(1, 0.0185791)
    iso.GenerateValues(2, [-1, 1])
    iso.Update()
    data = iso.GetOutput()
    print(data)
    write_to_vtk('output.vtk', data)

    # Create the mapper that corresponds the objects of the vtk file
    # into graphics elements
    mapper = vtk.vtkDataSetMapper()
    mapper.SetInputConnection(iso.GetOutputPort())

    # Create the Actor
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    # Create the Renderer
    renderer = vtk.vtkRenderer()
    renderer.AddActor(actor)
    # Set background to white
    colors = vtk.vtkNamedColors()
    renderer.SetBackground(colors.GetColor3d("SlateGray"))

    # Create the RendererWindow
    renderer_window = vtk.vtkRenderWindow()
    renderer_window.AddRenderer(renderer)

    # Create the RendererWindowInteractor and display the vtk_file
    interactor = vtk.vtkRenderWindowInteractor()
    interactor.SetRenderWindow(renderer_window)
    interactor.Initialize()
    interactor.Start()


def get_program_parameters():
    import argparse
    description = 'Marching cubes surface'
    epilogue = '''
    '''
    parser = argparse.ArgumentParser(description=description, epilog=epilogue,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('filename', help='./VTK/flow_VTK.vtk')
    args = parser.parse_args()
    return args.filename


def write_to_vtk(output_file_name, data):
    writer = vtk.vtkXMLPolyDataWriter()
    writer.SetInputData(data)
    writer.SetFileName(output_file_name)
    writer.Write()


if __name__ == '__main__':
    main()
