# Extract Isosurface using VTK 
Scripts in vtk_turbulent are used to extract isosurface from unstructured data and store the result in another vtk file

#### File Structure

  | -- -- vtk_turbulent
  
           | -- --- VTK 
  
           |           | -- -- channel395_55600
    
            |                       | -- -- boundary 
  
            |                                  | -- -- bottomWall.vtp
  
            |                                  | -- -- inout1_half0.vtp
  
            |                                  | -- -- inout1_half1.vtp
  
            |                                  | -- -- inout2_half0.vtp
  
            |                                  | -- -- inout2_half1.vtp
  
            |                                  | -- -- sides1_half0.vtp
  
            |                                  | -- -- sides1_half1.vtp
  
            |                                  | -- -- sides2_half0.vtp
    
            |                                  | -- -- sides2_half1.vtp
  
            |           | -- -- channel395.vtm.series
  
            |           | -- -- channel395_55600.vtm
  
            |           | -- -- flow_VTK.vtk
  
            | -- -- output.vtk

            | -- -- output_iso.py 

            | -- -- vtk_extract.py

            | -- -- VTKBlender.py 


* vtk_extarct.py: input an unstructured data vtk file, output the isosurface in output.vtk.
* output_iso.py: test the output is correct.
* VTKBlender.py: convert the vtk file to data that is readable by Blender.


#### Installation 
Project Environment:
1. Python > 3.0 (3.7)
2. Anaconda 3
3. conda install -c anaconda vtk 

#### Usage 
python3 vtk_extract.py 
python3 output_iso.py 
python3 VTKBlender.py 



